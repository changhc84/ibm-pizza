package lib

import (
	"errors"
	"fmt"
	"os"
	"time"
)

// ProcessOrder handles an order
func (p *MyPizzaBaker) ProcessOrder() Order {
	if order := <-p.orderCh; order != nil {
		time.Sleep(1 * time.Millisecond)
		return *order
	}
	return Order{isLastOrder: true}
}

// Prepare prepares a pizza from an order
func (p *MyPizzaBaker) Prepare(order Order) *Pizza {
	time.Sleep(2 * time.Millisecond)
	return &Pizza{orderID: order.ID()}
}

// QualityCheck conducts a quality check on a pizza
func (p *MyPizzaBaker) QualityCheck(pizza *Pizza) (*Pizza, error) {
	var err error
	time.Sleep(1 * time.Millisecond)
	if !pizza.isBaked {
		err = errors.New("Pizza is not baked")
	}
	return pizza, err
}

// CreateOrder creates an order
func (p *MyPizzaBaker) CreateOrder(order Order) {
	p.mutex.Lock()
	if p.closed {
		p.mutex.Unlock()
		return
	}
	p.orderID++
	order.id = p.orderID
	p.mutex.Unlock()
	p.orderCh <- &order
}

// Deliver returns a baked pizza. This is a blocking call.
func (p *MyPizzaBaker) Deliver() *Pizza {
	return <-p.readyPizzaCh
}

// Init initiates a baker
func (p *MyPizzaBaker) Init(capacity, ovenCount uint) {
	if capacity == 0 {
		fmt.Printf("Capacity = 0: I don't take any order today :D\n")
		os.Exit(1)
	}
	if ovenCount == 0 {
		fmt.Printf("Error: I need at least one oven\n")
		os.Exit(1)
	}
	p.capacity = capacity
	p.bakedPizzaCh = make(chan *Pizza, capacity)
	p.readyPizzaCh = make(chan *Pizza, capacity)
	p.orderCh = make(chan *Order, capacity)
	p.availableOvenCh = make(chan *MyOven, ovenCount)
	p.ovens = []Oven{}

	for i := uint(0); i < ovenCount; i++ {
		oven := MyOven{id: i, bakerCh: p.bakedPizzaCh, ovenCh: p.availableOvenCh, bakeTime: 5 * time.Millisecond}
		p.ovens = append(p.ovens, oven)
		p.availableOvenCh <- &oven
	}
}

// GetAvailableOven returns the oven available
func (p *MyPizzaBaker) GetAvailableOven() *MyOven {
	return <-p.availableOvenCh
}

// WatchOvens watches pizza just taken out from ovens
func (p *MyPizzaBaker) WatchOvens() {
	for pizza := range p.bakedPizzaCh {
		if okPizza, err := p.QualityCheck(pizza); err == nil {
			p.mutex.Lock()
			if p.closed {
				return
			}
			p.readyPizzaCh <- okPizza
			p.mutex.Unlock()
		} else {
			fmt.Println(err.Error())
		}
	}
}

// StartWorking launches the baker
func (p *MyPizzaBaker) StartWorking() {
	go p.WatchOvens()

	for {
		order := p.ProcessOrder()
		if order.isLastOrder {
			return
		}
		pizza := p.Prepare(order)
		oven := p.GetAvailableOven()
		go oven.Bake(*pizza)
	}
}

// StopWorking tells the baker to stop
func (p *MyPizzaBaker) StopWorking() {
	p.mutex.Lock()
	p.closed = true
	p.mutex.Unlock()

	// Wait until no oven is in use
	count := 0
	for count != len(p.ovens) {
		select {
		case <-p.availableOvenCh:
			count++
		// Discard anything left and drain the channels
		case <-p.bakedPizzaCh:
		case <-p.orderCh:
		default:
			continue
		}
	}

	close(p.orderCh)
	close(p.readyPizzaCh)
	close(p.bakedPizzaCh)
	close(p.availableOvenCh)
}
