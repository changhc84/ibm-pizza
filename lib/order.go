package lib

// ID returns the ID of the order
func (o Order) ID() uint {
	return o.id
}
