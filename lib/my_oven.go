package lib

import "time"

// Bake implements Oven.Bake
func (m MyOven) Bake(pizza Pizza) Pizza {
	time.Sleep(m.bakeTime)

	pizza.isBaked = true
	m.bakerCh <- &pizza
	m.ovenCh <- &m
	return pizza
}
