package lib

// OrderID returns the order ID of a pizza
func (p Pizza) OrderID() uint {
	return p.orderID
}
