package lib

import (
	"sync"
	"time"
)

// Order defines the content of an order
type Order struct {
	id uint

	// indicates if this is the last order (NIL)
	isLastOrder bool
}

// Pizza defines the properties of a pizza
type Pizza struct {
	isBaked bool

	orderID uint
}

// Oven defines what an oven does
type Oven interface {
	Bake(unbakedPizza Pizza) Pizza
}

// MyOven implements Oven
type MyOven struct {
	// ID of the oven
	id uint
	// the amount of time needed to bake a pizza
	bakeTime time.Duration
	// channel for baked pizzas
	bakerCh chan *Pizza
	// channel for available ovens
	ovenCh chan *MyOven
}

// PizzaBaker defines what a pizza baker does
type PizzaBaker interface {
	ProcessOrder() Order
	Prepare(order Order) *Pizza
	QualityCheck(pizza *Pizza) (*Pizza, error)
}

// MyPizzaBaker implements PizzaBaker
type MyPizzaBaker struct {
	// indicates if the baker has stopped working
	closed bool
	// keeps the last accepted order ID
	orderID uint
	// the max amount of pizzas the baker can handle simultaneously
	capacity uint
	// maintains the ready-to-serve pizzas
	readyPizzaCh chan *Pizza
	// maintains the pizzas just taken out from ovens
	bakedPizzaCh chan *Pizza
	// maintains incoming orders
	orderCh chan *Order
	// maintains ovens not in use
	availableOvenCh chan *MyOven
	// maintains all ovens owned by the baker
	ovens []Oven
	// just a mutex
	mutex sync.Mutex
}
