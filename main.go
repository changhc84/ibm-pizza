package main

import (
	"flag"
	"fmt"
	"ibm-pizza/lib"
	"os"
	"os/signal"
	"runtime/pprof"
	"runtime/trace"
	"syscall"
	"time"
)

var traceFile *os.File
var pprofFile *os.File

func cleanUp(baker *lib.MyPizzaBaker, done chan bool) {
	baker.StopWorking()
	trace.Stop()
	traceFile.Close()

	if pprofFile != nil {
		pprof.StopCPUProfile()
		pprofFile.Close()
	}
	done <- true
}

func setup(baker *lib.MyPizzaBaker, done chan bool, pprofEnable bool) {
	traceFile, _ = os.Create("trace.out")
	trace.Start(traceFile)
	if pprofEnable {
		pprofFile, _ = os.Create("pprof.out")
		pprof.StartCPUProfile(pprofFile)
	}
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		cleanUp(baker, done)
	}()
}

func main() {
	nOrder := flag.Uint("nOrder", uint(20), "number of orders")
	bakerCapacity := flag.Uint("capacity", uint(10), "capacity of the baker")
	nOven := flag.Uint("nOven", uint(20), "number of ovens the baker has")
	pprofEnable := flag.Bool("pprof", false, "enable pprof")
	flag.Parse()

	// Setup baker
	done := make(chan bool)
	baker := lib.MyPizzaBaker{}

	setup(&baker, done, *pprofEnable)
	baker.Init(*bakerCapacity, *nOven)
	go baker.StartWorking()

	startTime := time.Now()
	// Pick up baked pizzas
	go func(baker *lib.MyPizzaBaker) {
		defer cleanUp(baker, done)
		for i := uint(0); i < *nOrder; i++ {
			baker.Deliver()
			/*
				pizza := baker.Deliver()
				if pizza == nil {
					fmt.Println("Got kicked out by the baker :(")
					return
				}
				fmt.Printf("Got baked pizza %d\n", pizza.OrderID())
			*/
		}
		timeDiff := time.Now().Sub(startTime)
		fmt.Println("All good!")
		fmt.Printf("Time spent for %d pizza(s) with capacity %d and %d oven(s): %v\n", *nOrder, *bakerCapacity, *nOven, timeDiff)
		fmt.Printf("It took %.3f ms to serve 1 pizza.\n", float64(timeDiff.Nanoseconds())/float64(*nOrder)/1e6)
	}(&baker)

	// Create orders
	for i := uint(0); i < *nOrder; i++ {
		baker.CreateOrder(lib.Order{})
	}

	<-done
}
