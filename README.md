# Pizza Maker
Let's bake some pizza. Customers order pizzas and wait for it.
 
## Making process
The sequential order of the pizza production process comprises 4 steps.
Each production step requires some time (in milliseconds) to finish.
A customer submits an order and waits for delivery. Once a pizza baker
received an order, he/she prepares the pizza, bakes it, and eventually
checks the quality of the baked product and delivers it.
 
- Receive and process order (1ms)
- Prepare pizza (2ms)
- Bake (5ms)
- Quality check (1ms)
 
We propose the following interfaces:
 
```go
type Order struct {
}
type Pizza struct {
    isBaked bool
}
type Oven interface {
    Bake(unbakedPizza Pizza) Pizza
}
type PizzaBaker interface {
    ProcessOrder() Order
    Prepare(order) *Pizza
    QualityCheck(pizza *Pizza) (*Pizza, error)
}
```
 
## Your Task
 
Implement a prototype and evaluate the performance (pizza throughput and latency) of your Pizza maker implementation. Please document the resources you are using.

## Implementation

My implementation is written in Go. For simplicity, the methods proposed do not actually perform any action. They just sleep for the time specified.

### Design
![](./img/pizzas.png)

In this design there are two people/workers actually: one for making pizzas and one for quality check. The baker can accept multiple orders at the same time. However, he/she can only focus on making one raw pizza each time. The baker has multiple ovens, say `nOven`, as well. On the other hand, the person for quality check only handles one pizza at a time.

In this model I made some assumption:

- The baker do not accept orders if there are already `capacity` pizzas to be baked. He/she does not have enough space to store raw pizzas. As soon as a pizza is taken out from an oven, he/she starts to accept the next order.
- The baker do not accept orders if there are already `capacity` baked pizzas to be collected. He/she doesn't want ready pizzas to pile up if no one collects them.
- After the baker puts a raw pizza into an oven, he/she can start to make the next raw pizza.

### Throughput
Throughput is calculated in a naive way: divide the number of orders by the time elapsed.

![](./img/throughput.png)

It shows that the latency stablises at ~3.3ms (throughput ~ 300 pizzas/s).

### Monitoring

To find out which parts of the program could be the performance bottleneck, I use [runtime/trace](https://pkg.go.dev/runtime/trace) and [runtime/pprof](https://pkg.go.dev/runtime/pprof) where the number of calls and call duration are logged.

![](./img/bake-trace-normal.png)


This screenshot shows the execution time of each bake call. In my implementation, each bake runs in a new goroutine so that it does not block the baker. We can see that the lifetime of each goroutine is roughly 5ms as expected.

![](./img/bake-trace.png)


The main blocking calls are for waiting for pizzas to be delivered or orders to be accepted.

![](./img/baker-main-trace.png)


If we add some CPU-intensive work to somewhere in the code, e.g., adding the following loop to `QualityCheck`:
```go
for i := 0; i < 10000000; i++ {
}
```
from the trace we can see that indeed `QualityCheck` takes a lot more time.

![](./img/bake-trace-loop.png)

This is also reflected in `pprof`:

![](./img/pprof.png)

### Run the code

Tested with

- Ubuntu 20.04
- go1.15 linux/amd64

```sh
./run.sh
```

Sample execution
```
$ ./run.sh
All good!                                                                  
Time spent for 1000 pizza(s) with capacity 10 and 20 oven(s): 3.304663008s 
It took 3.305 ms to serve 1 pizza.
```

#### Check trace and pprof output
Sample output of `trace` and `pprof` are placed in `./sample`.

```sh
# Dump pprof info into a png file
go tool pprof -png -output <output name> ./sample/pprof.out

# Visualise trace
go tool trace ./sample/trace.out
```


## Reference

- https://making.pusher.com/go-tool-trace/
- https://go.dev/blog/pprof
- `StackOverflow` in general
